from django.shortcuts import render, redirect, HttpResponse
from .forms import CreateTransaksi,UpdateTransaksi,UpdateStatus
from django.db import connection
from datetime import date, datetime
import time
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.

def view_transaksi(request):
    if "email" not in request.session:
        return redirect('/auth/login')

    transaksi_raw = get_transaksi()
    response = {}
    response['is_staf'] = "is_staf" in request.session
    response['data'] = transaksi_raw
    return render(request,'transaksilaundry/view_transaksi.html',response)


def view_status(request):
    if "email" not in request.session:
        return redirect('/auth/login')

    status_transaksi_raw = get_status_transaksi()
    response = {}
    response['is_staf'] = "is_staf" in request.session
    response['data'] = status_transaksi_raw
    return render(request,'transaksilaundry/view_status.html',response)

@csrf_exempt
def update_transaksi(request,email,tanggal):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')
    
    if(email != None):
        prefilled_data = get_email_pengguna_update(email,tanggal)
        response={}
        response['error']=False
        form = UpdateTransaksi(request.POST or None, initial={'email':email,'tanggal':tanggal,'berat':prefilled_data['berat'],'total_item':prefilled_data['total_item'],'kode_layanan':prefilled_data['kode_layanan'],'kode_tarif':prefilled_data['kode_tarif'],'Total_Biaya':prefilled_data['total_harga'],'email_staf':prefilled_data['email_staf'],'email_kurir':prefilled_data['email_kurir_antar']})
        form.fields['email'].disabled = True
        form.fields['tanggal'].disabled = True
        email = get_email_pengguna()
        kode_layanan = get_kode_layanan()
        kode_tarif = get_kode_tarif()
        email_staf = get_email_staf()
        email_kurir = get_email_kurir()  
        form.fields['email'].choices=email
        form.fields['kode_layanan'].choices=kode_layanan
        form.fields['kode_tarif'].choices=kode_tarif
        form.fields['email_staf'].choices=email_staf
        form.fields['email_kurir'].choices=email_kurir
        response['form'] = form
        
    if(request.method == "POST" and form.is_valid()):
        email = form.cleaned_data['email']
        tanggal=form.cleaned_data['tanggal']
        berat = form.cleaned_data['berat']
        total_item = form.cleaned_data['total_item']
        kode_layanan = form.cleaned_data['kode_layanan']
        kode_tarif= form.cleaned_data['kode_tarif']
        Total_Biaya=form.cleaned_data['Total_Biaya']
        email_staf=form.cleaned_data['email_staf']
        email_kurir=form.cleaned_data['email_kurir']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("update transaksi_laundry set email=%s, tanggal=%s, berat=%s, total_item=%s, kode_layanan=%s, kode_tarif=%s , total_harga=%s, email_staf=%s , email_kurir_antar=%s where email=%s and tanggal=%s", [email,tanggal,berat,total_item,kode_layanan,kode_tarif,Total_Biaya,email_staf,email_kurir,email,tanggal])
        return redirect('transaksilaundry:view_transaksi')

    return render(request, 'transaksilaundry/update_transaksi.html', {'form': form})

def delete_transaksi_laundry(request,email,tanggal):
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("delete from transaksi_laundry where email=%s and tanggal=%s;",[email,tanggal])
        return redirect('transaksilaundry:view_transaksi')


def update_status(request,email,tanggal_transaksi):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')
    
    if(email != None):
        prefilled_data = get_email_pengguna_update_status(email,tanggal_transaksi)
        response={}
        response['error']=False
        form = UpdateStatus(request.POST or None, initial={'email':email,'kode_status':prefilled_data['kode_status'],'timestamp':prefilled_data['timestamp']})
        form.fields['email'].disabled = True
        email = get_email_pengguna()
        kode_status = get_kode_status()
        form.fields['email'].choices=email
        form.fields['kode_status'].choices=kode_status
        response['form'] = form
        
    if(request.method == "POST" and form.is_valid()):
        email = form.cleaned_data['email']
        kode_status = form.cleaned_data['kode_status']
        timestamp=form.cleaned_data['timestamp']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("update status_transaksi set email=%s, kode_status=%s, timestamp=%s where email=%s and tanggal_transaksi=%s", [email,kode_status,timestamp,email,tanggal_transaksi])
        return redirect('transaksilaundry:view_status')

    return render(request, 'transaksilaundry/update_status.html', {'form': form})


def delete_status_transaksi_laundry(request,email,tanggal_transaksi):
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("delete from status_transaksi where email=%s and tanggal_transaksi=%s;",[email,tanggal_transaksi])
        return redirect('transaksilaundry:view_status')
    
def create_transaksi(request):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')
    response = {}
    response['error'] = False
    email = get_email_pengguna()
    kode_layanan = get_kode_layanan()
    kode_tarif = get_kode_tarif()
    email_staf = get_email_staf()
    email_kurir = get_email_kurir()
    form = CreateTransaksi(request.POST or None)   
    form.fields['email'].choices=email
    form.fields['kode_layanan'].choices=kode_layanan
    form.fields['kode_tarif'].choices=kode_tarif
    form.fields['email_staf'].choices=email_staf
    form.fields['email_kurir'].choices=email_kurir
    response['form'] = form
    
    if(request.method=="POST" and form.is_valid()):
        print(form.cleaned_data)
        email = form.cleaned_data['email']
        berat = form.cleaned_data['berat']
        total_item = form.cleaned_data['total_item']
        diskon =0
        kode_layanan = form.cleaned_data['kode_layanan']
        kode_tarif= form.cleaned_data['kode_tarif']
        email_staf=form.cleaned_data['email_staf']
        email_kurir=form.cleaned_data['email_kurir']
        harga=get_harga_layanan(kode_layanan)
        biaya_laundry=harga['harga']
        tanggal = date.today()
        waktu=datetime.now().time()
        biaya_antar=get_harga_tarif(kode_tarif)['harga']
        biaya_jemput=get_harga_tarif(kode_tarif)['harga']  
        Total_Biaya=form.cleaned_data['Total_Biaya']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("insert into transaksi_laundry(email,tanggal,berat,total_item,biaya_laundry,biaya_antar,biaya_jemput,diskon,total_harga,foto_pengambilan,kode_layanan,kode_tarif,email_staf,email_kurir_antar,email_kurir_jemput) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",[email,tanggal,berat,total_item,biaya_laundry,biaya_antar,biaya_jemput,diskon,Total_Biaya,'http://dummyimage.com/213x199.bmp/5fa2dd/ffffff',kode_layanan,kode_tarif,email_staf,email_kurir,email_kurir])
        cursor.execute("insert into status_transaksi(email,tanggal_transaksi,kode_status,timestamp) values(%s,%s,%s,%s);",[email,tanggal,'S01',waktu])
        return redirect('transaksilaundry:view_transaksi')
    return render(request, 'transaksilaundry/create_transaksi.html', response)
    

def get_harga_layanan(kode):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select harga from layanan where kode=%s;",[kode])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_harga_tarif(kode):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select harga from tarif_antar_jemput where kode=%s;",[kode])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_email_pengguna_update(email,tanggal):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from transaksi_laundry where email=%s and tanggal=%s;",[email,tanggal])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_email_pengguna_update_status(email,tanggal):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from status_transaksi where email=%s and tanggal_transaksi=%s;",[email,tanggal])
    res_dirty = fetch(cursor)
    return res_dirty[0]
    
def get_email_pengguna():
    query="""
    select email from pelanggan;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    email_response = fetch(cursor)
    res=[]
    for email in email_response:
        temp = email['email']
        res.append((temp,temp,))
    return res


def get_kode_layanan():
    query="""
    select kode from layanan;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    kode_layanan_response = fetch(cursor)
    res=[]
    for kode in kode_layanan_response:
        temp = kode['kode']
        res.append((temp,temp,))
    return res

def get_kode_status():
    query="""
    select kode from status;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    kode_status_response = fetch(cursor)
    res=[]
    for kode in kode_status_response:
        temp = kode['kode']
        res.append((temp,temp,))
    return res

def get_kode_tarif():
    query="""
    select kode from tarif_antar_jemput;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    kode_tarif_antar_jemput_response = fetch(cursor)
    res=[]
    for kode in kode_tarif_antar_jemput_response:
        temp = kode['kode']
        res.append((temp,temp,))
    return res
def get_email_staf():
    query="""
    select email from staf;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    email_staf_response = fetch(cursor)
    res=[]
    for email in email_staf_response:
        temp = email['email']
        res.append((temp,temp,))
    return res

def get_email_kurir():
    query="""
    select email from kurir;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    email_kurir_response = fetch(cursor)
    res=[]
    for email in email_kurir_response:
        temp = email['email']
        res.append((temp,temp,))
    return res


def get_transaksi():
    query="""
    select *
	from transaksi_laundry tl
	order by tl.tanggal asc;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    transaksi_response = fetch(cursor)
    return transaksi_response

def get_status_transaksi():
    query="""
    select *
	from status_transaksi st
	order by st.tanggal_transaksi asc;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    status_transaksi_response = fetch(cursor)
    return status_transaksi_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]



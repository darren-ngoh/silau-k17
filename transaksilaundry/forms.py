from django import forms

class CreateTransaksi(forms.Form):
    email = forms.ChoiceField(label="Email",choices=[], help_text='\n')
    berat = forms.IntegerField(label="Berat",help_text='\n')
    total_item = forms.IntegerField(label="Total Item", help_text='\n')
    kode_layanan = forms.ChoiceField(label="Kode Layanan", choices=[], help_text='\n')
    kode_tarif= forms.ChoiceField(label="Kode Tarif", choices=[], help_text='\n')
    Total_Biaya=forms.IntegerField(label="Total Biaya", help_text='\n')
    email_staf=forms.ChoiceField(label="Email Staf", choices=[], help_text='\n')
    email_kurir=forms.ChoiceField(label="Email Kurir", choices=[], help_text='\n')
    
class UpdateTransaksi(forms.Form):
    email = forms.ChoiceField(label="Email", choices=[], help_text='\n')
    tanggal= forms.DateField(label="Tanggal", help_text='\n')
    berat = forms.IntegerField(label="Berat", help_text='\n')
    total_item = forms.IntegerField(label="Total Item", help_text='\n')
    kode_layanan = forms.ChoiceField(label="Kode Layanan", choices=[], help_text='\n')
    kode_tarif= forms.ChoiceField(label="Kode Tarif", choices=[], help_text='\n')
    Total_Biaya=forms.IntegerField(label="Total Biaya", help_text='\n')
    email_staf=forms.ChoiceField(label="Email Staf", choices=[], help_text='\n')
    email_kurir=forms.ChoiceField(label="Email Kurir", choices=[], help_text='\n')
        
class UpdateStatus(forms.Form):
    email = forms.ChoiceField(label="Email", choices=[], help_text='\n')
    kode_status= forms.ChoiceField(label="kode Status", choices=[], help_text='\n')
    timestamp= forms.TimeField(label="Timestamp", help_text='\n')

        
        
        

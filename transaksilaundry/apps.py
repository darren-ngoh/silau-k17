from django.apps import AppConfig


class TransaksilaundryConfig(AppConfig):
    name = 'transaksilaundry'

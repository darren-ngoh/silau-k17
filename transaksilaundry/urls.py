from django.urls import path

from . import views

app_name = "transaksilaundry"

urlpatterns = [
    path('', views.view_transaksi,name='view_transaksi'),
    path('update_transaksi/<str:email>,<str:tanggal>', views.update_transaksi, name='update_transaksi'),
    path('delete_transaksi/<str:email>,<str:tanggal>', views.delete_transaksi_laundry, name='delete_transaksi'),
    path('create/', views.create_transaksi, name='create_transaksi'),
    path('view_status/', views.view_status, name='view_status'),
    path('update_status/<str:email>,<str:tanggal_transaksi>', views.update_status, name='update_status'),
    path('delete_status/<str:email>,<str:tanggal_transaksi>', views.delete_status_transaksi_laundry, name='delete_status'),
    
]

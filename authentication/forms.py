from django import forms

class LoginForm(forms.Form):
    email=forms.CharField(label='email',max_length=20)
    password=forms.CharField(widget=forms.PasswordInput)

class RegisterFormStaf(forms.Form):
    email=forms.EmailField(label="Email",max_length=50)
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nama_lengkap=forms.CharField(label='Nama Lengkap',max_length=50)
    no_hp=forms.CharField(label='Nomor Telepon', max_length=20)
    jenis_kelamin= forms.CharField(label="Jenis Kelamin(F/M)",max_length=1)
    alamat= forms.CharField(label="alamat",required=False)
    npwp=forms.CharField(label='NPWP', max_length=20)
    no_rekening=forms.CharField(label='No Rekening', max_length=20)
    nama_bank=forms.CharField(label='Nama Bank',max_length=50)
    kantor_cabang=forms.CharField(label='Nama Kantor Cabang',max_length=50)

class RegisterFormPelanggan(forms.Form):
    email=forms.EmailField(label="Email",max_length=50)
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nama_lengkap=forms.CharField(label='Nama Lengkap',max_length=50)
    no_hp=forms.CharField(label='Nomor Telepon', max_length=20)
    jenis_kelamin= forms.CharField(label="Jenis Kelamin(F/M)",max_length=1)
    tanggal_lahir = forms.DateTimeField()
    alamat= forms.CharField(label="alamat contoh([Rumah]kukusan23; [kost] jl.margonda)",required=False)

class RegisterFormKurir(forms.Form):
    email=forms.EmailField(label="Email",max_length=50)
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nama_lengkap=forms.CharField(label='Nama Lengkap',max_length=50)
    no_hp=forms.CharField(label='Nomor Telepon', max_length=20)
    jenis_kelamin= forms.CharField(label="Jenis Kelamin(F/M)",max_length=1)
    alamat= forms.CharField(label="alamat",required=False)
    npwp=forms.CharField(label='NPWP', max_length=20)
    no_rekening=forms.CharField(label='No Rekening', max_length=20)
    nama_bank=forms.CharField(label='Nama Bank',max_length=50)
    kantor_cabang=forms.CharField(label='Nama Kantor Cabang',max_length=50)
    no_sim=forms.CharField(label='Nomor SIM', max_length=20)
    nomor_kendaraan=forms.CharField(label='Nomor Kendaraan', max_length=20)
    jenis_kendaraan=forms.CharField(label='Jenis Kendaraan', max_length=20)


from django.shortcuts import render, redirect
from django.db import connection
from .forms import LoginForm,RegisterFormStaf, RegisterFormKurir, RegisterFormPelanggan 
# Create your views here. 
def login(request):
    if "email" in request.session:
        return redirect('/')
    response = {}
    response['error'] = False

    form = LoginForm(request.POST or None)
    response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        if(verified(response)):
            request.session['email'] = response['email']
            if(is_staf(response['email'])):
                request.session['is_staf'] = True
            elif(is_kurir(response['email'])):
                request.session['is_kurir'] = True
            else:
                request.session['is_user']= True
            return redirect('/auth/authentication-success')
        else:
            response['error'] = True

    return render(request,'login.html',response)

def logout(request):
    if "email" in request.session:
        request.session.flush()
        return redirect('/')
    return redirect('/')

def authentication_success(request):
    if "email" in request.session:
        return render(request,'success.html')
    else:
        return redirect('/')

def verified(data):
    query = """
    select * from pengguna;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)

    user_data = fetch(cursor)
    for users in user_data:
        email_status = data['email'] == users['email']
        password_status = data['password'] == users['password']
        if(email_status and password_status):
            return True
    return False

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def is_email_taken(email):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select email from pengguna;")
    all_registered_email = fetch(cursor)
    result = False
    for emails in all_registered_email:
        if emails['email'] == email:
            result = True
            break
    return result


def is_staf(email_input):
    # fetch semua nomor_pegawai dari admin
    query1 = """
    select email from staf;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query1)

    email_staf_response = fetch(cursor)
    for email in email_staf_response:
        if(email_input == email['email']):
            return True
    return False

def is_kurir(email_input):
    # fetch semua nomor_pegawai dari admin
    query1 = """
    select email from kurir;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query1)

    email_staf_response = fetch(cursor)
    for email in email_staf_response:
        if(email_input == email['email']):
            return True
    return False

def unauthorized(request):
    if "email" in request.session:
        return render(request,'unauthorized.html')
    else:
        return redirect('/')

def register(request):
    if "email" in request.session:
        return redirect('/')
    return render(request,'register.html')

def register_staf(request):
    if "email" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormStaf(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_hp = request.POST['no_hp']
        jenis_kelamin = request.POST['jenis_kelamin']
        alamat = request.POST['alamat']
        npwp = request.POST['npwp']
        no_rekening = request.POST['no_rekening']
        nama_bak = request.POST['nama_bank']
        kantor_cabang = request.POST['kantor_cabang']
        if(not is_email_taken(email)):
            insert_pengguna(email,password,nama_lengkap,alamat,no_hp,jenis_kelamin)
            insert_staf(email,npwp,no_rekening,nama_bank,kantor_cabang)
            return redirect('/auth/register/success')
        else:
            response['error'] = True
    return render(request,'register_staf.html',response)


def insert_pengguna(email,password,nama_lengkap,alamat,no_hp,jenis_kelamin):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("INSERT INTO PENGGUNA (email,password,nama_lengkap,alamat,no_hp,jenis_kelamin) values (%s,%s,%s,%s,%s,%s)",[email,password,nama_lengkap,alamat,no_hp,jenis_kelamin])

def insert_staf(email,npwp,no_rekening,nama_bank,kantor_cabang):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("INSERT INTO staf (email,npwp,no_rekening,nama_bank,kantor_cabang) values (%s,%s,%s,%s,%s)",[email,npwp,no_rekening,nama_bank,kantor_cabang])

def insert_kurir(email,npwp,no_rekening,nama_bank,kantor_cabang,no_sim,nomor_kendaraan,jenis_kendaraan):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("insert into kurir (email,npwp,no_rekening,nama_bank,kantor_cabang,no_sim,nomor_kendaraan,jenis_kendaraan) values(%s,%s,%s,%s,%s,%s,%s,%s)",[email,npwp,no_rekening,nama_bank,kantor_cabang,no_sim,nomor_kendaraan,jenis_kendaraan])

def insert_pelanggan(email):
    # asuransi defaults to manulife
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("insert into pelanggan (email,no_virtual_account,saldo_dpay) values(%s,%s,%s)",[email,get_new_nomor_va(),0])

def register_kurir(request):
    if "email" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormKurir(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_hp = request.POST['no_hp']
        jenis_kelamin = request.POST['jenis_kelamin']
        alamat = request.POST['alamat']
        npwp = request.POST['npwp']
        no_rekening = request.POST['no_rekening']
        nama_bank = request.POST['nama_bank']
        kantor_cabang = request.POST['kantor_cabang']
        no_sim = request.POST['no_sim']
        nomor_kendaraan = request.POST['nomor_kendaraan']
        jenis_kendaraan = request.POST['jenis_kendaraan']
        if(not is_email_taken(email)):
            insert_pengguna(email,password,nama_lengkap,alamat,no_hp,jenis_kelamin)
            insert_kurir(email,npwp,no_rekening,nama_bank,kantor_cabang,no_sim,nomor_kendaraan,jenis_kendaraan)
            return redirect('/auth/register/success')
        else:
            response['error'] = True
    return render(request,'register_kurir.html',response)

def register_pelanggan(request):
    if "email" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormPelanggan(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        print(request.POST)
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_hp = request.POST['no_hp']
        jenis_kelamin = request.POST['jenis_kelamin']
        tanggal_lahir = request.POST['tanggal_lahir']
        alamat = request.POST['alamat']
        if(not is_email_taken(email)):
            insert_pengguna(email,password,nama_lengkap,alamat,no_hp,jenis_kelamin)
            insert_pelanggan(email)
            return redirect('/auth/register/success')
        else:
            response['error'] = True
    return render(request,'register_pelanggan.html',response)


def register_success(request):
    return render(request,'register-success.html')

def get_new_nomor_va():
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select no_virtual_account from pelanggan;")
    all_va = fetch(cursor)
    temp = []
    for va in all_va:
        temp.append(va['no_virtual_account'])
    result = int(float(max(temp))) + 10
    return result

def test(request):
    if "email" not in request.session:
        return redirect('/')
    return render(request,'test.html')
from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='authentication' 

urlpatterns = [
    path('login/',views.login,name='login'),
    path('authentication-success/',views.authentication_success,name='authentication-success'),
    path('logout/',views.logout,name='logout'),
    path('unauthorized/',views.unauthorized,name='unauthorized'),
    path('register/',views.register,name='register'),
    path('register/success',views.register_success,name='register-success'),
    path('register/staf',views.register_staf,name='register-staf'),
    path('register/kurir',views.register_kurir,name='register-kurir'),
    path('register/pelanggan',views.register_pelanggan,name='register-pelanggan'),
    path('test/',views.test,name='test'),
]

from django import forms

class UpdateLayanan(forms.Form):
    kode=forms.CharField(label="Kode",max_length=10)
    nama=forms.CharField(label='Nama Layanan',max_length=50)
    durasi=forms.CharField(label='Durasi (Hari)',max_length=10)
    harga=forms.IntegerField(label='Harga')
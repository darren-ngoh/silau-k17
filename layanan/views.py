from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import UpdateLayanan
import json
from django.views.decorators.csrf import csrf_exempt

def display_layanan(request):
    if "email" not in request.session:
        return redirect('/auth/login')
    response={}
    response['data'] = get_layanan()
    return render(request,'display-layanan.html',response)

def get_layanan():
    query = """
    select * from layanan;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    layanan_response = fetch(cursor)
    return layanan_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def update_layanan(request,kode):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if(kode != None):
        prefilled_data = get_data(kode)
        response={}
        response['error']=False
        form = UpdateLayanan(request.POST or None, initial={'kode':kode,'nama':prefilled_data['nama'],'durasi':prefilled_data['durasi'],'harga':prefilled_data['harga']})
        form.fields['kode'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        kode = form.cleaned_data['kode']
        nama = form.cleaned_data['nama']
        durasi = form.cleaned_data['durasi']
        harga = form.cleaned_data['harga']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("update layanan set nama=%s, durasi=%s, harga=%s where kode=%s",[nama,durasi,harga,kode])
        return redirect("/layanan")
    return render(request,'update-layanan.html',response)

def get_data(kode):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from layanan where kode=%s;",[kode])
    res_dirty = fetch(cursor)
    return res_dirty[0]
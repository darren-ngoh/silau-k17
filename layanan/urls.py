from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='layanan'

urlpatterns = [ 
	path('',views.display_layanan,name='display-layanan'), 
	path('update/<str:kode>',views.update_layanan,name='update-layanan'), 
] 
 
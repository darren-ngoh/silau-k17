from django import forms

class UpdateTestimoni(forms.Form):
    email=forms.EmailField(label='Email')
    tanggal_transaksi=forms.DateField(label='Tanggal Transaksi')
    status=forms.CharField(label='Status',max_length=100)
    rating=forms.ChoiceField(label='Rating',choices=[])

class CreateTestimoni(forms.Form):
    email=forms.ChoiceField(label='Email',choices=[])
    tanggal_transaksi=forms.ChoiceField(label='Tanggal Transaksi',choices=[])
    status=forms.CharField(label='Status',max_length=100)
    rating=forms.ChoiceField(label='Rating',choices=[])
from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='testimoni'

urlpatterns = [ 
	path('',views.display_testimoni,name='display-testimoni'), 
	path('create/',views.create_testimoni,name='create-testimoni'), 
	path('update/<str:email>/<str:tanggal_transaksi>',views.update_testimoni,name='update-testimoni'), 
	path('delete/<str:email>/<str:tanggal_transaksi>',views.delete_testimoni,name='delete-testimoni'),
] 
 
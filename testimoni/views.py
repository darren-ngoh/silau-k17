from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import UpdateTestimoni, CreateTestimoni
import json
from django.views.decorators.csrf import csrf_exempt
from datetime import date

def create_testimoni(request):
    if "is_user" not in request.session:
        return redirect('/auth/unauthorized') 
    if "email" not in request.session:
        return redirect('/auth/login')

    response={}
    response['error']=False
    ratingku=get_rating()
    emailku=get_all_email()
    tanggalku=get_all_tanggal_transaksi(request)
    form = CreateTestimoni(request.POST or None)
    form.fields['rating'].choices=ratingku
    form.fields['email'].choices=emailku
    form.fields['tanggal_transaksi'].choices=tanggalku
    response['form'] = form
    
    if(request.method == "POST" and form.is_valid()):
        print(form.cleaned_data)
        email = form.cleaned_data['email']
        status = form.cleaned_data['status']
        tanggal_transaksi = form.cleaned_data['tanggal_transaksi']
        rating = form.cleaned_data['rating']
        tanggal_testimoni = date.today()
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("insert into testimoni (email,tanggal_transaksi,tanggal_testimoni,status,rating) values(%s,%s,%s,%s,%s);",[email,tanggal_transaksi,tanggal_testimoni,status,rating])
        return redirect('/testimoni') 
    return render(request,'create-testimoni.html',response)

def display_testimoni(request):
    if "is_kurir" in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')

    if "is_user" in request.session:
        response={}
        response['data'] = get_testimoni(request)
        return render(request,'display-testimoni.html',response)
    
    else:
        response={}
        response['data'] = get_testimoni_staf()
        return render(request,'display-testimoni.html',response)

def get_testimoni_staf():
    query = """
    select * from testimoni;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    testimoni_response = fetch(cursor)
    return testimoni_response

def get_testimoni(request):
    email = request.session["email"]
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from testimoni where email=%s;",[email])
    testimoni_response = fetch(cursor)
    return testimoni_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_testimoni(request,email,tanggal_transaksi):
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("delete from testimoni where email=%s and tanggal_transaksi=%s;",[email,tanggal_transaksi])
        return redirect("/testimoni")

@csrf_exempt
def update_testimoni(request,email,tanggal_transaksi):
    if "is_kurir" in request.session:
        return redirect('/auth/unauthorized')
    if(email != None):
        prefilled_data = get_data(email,tanggal_transaksi)
        response={}
        response['error']=False
        ratingku = get_rating()
        form = UpdateTestimoni(request.POST or None, initial={'email':email,'tanggal_transaksi':tanggal_transaksi,'tanggal_testimoni':prefilled_data['tanggal_testimoni'],'status':prefilled_data['status'],'rating':prefilled_data['rating']})
        form.fields['rating'].choices=ratingku
        form.fields['email'].disabled = True
        form.fields['tanggal_transaksi'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        email = form.cleaned_data['email']
        tanggal_transaksi = form.cleaned_data['tanggal_transaksi']
        tanggal_testimoni = date.today()
        status = form.cleaned_data['status']
        rating = form.cleaned_data['rating']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("update testimoni set rating=%s, status=%s , tanggal_testimoni=%s where email=%s and tanggal_transaksi=%s",[rating,status,tanggal_testimoni,email,tanggal_transaksi])
        return redirect("/testimoni")
    return render(request,'update-testimoni.html',response)

def get_data(email,tanggal_transaksi):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from testimoni where email=%s and tanggal_transaksi=%s",[email,tanggal_transaksi])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_rating():
    res=[]
    for i in range(1, 6):
        temp= i
        res.append((temp,temp,))
    return res

def get_all_email():
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select email from transaksi_laundry;")
    emails_response = fetch(cursor)
    res=[]
    for email in emails_response:
        temp=email['email']
        res.append((temp,temp,))
    return res

def get_all_tanggal_transaksi(request):
    email = request.session["email"]
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select tanggal from transaksi_laundry where email=%s;",[email])
    tanggal_response = fetch(cursor)
    res=[]
    for tanggal in tanggal_response:
        temp=tanggal['tanggal']
        res.append((temp,temp,))
    return res
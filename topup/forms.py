from django import forms

class CreateTopup(forms.Form):
    email = forms.ChoiceField(label="Email", choices=[])
    nominal = forms.IntegerField(label="Nominal")

class UpdateTopup(forms.Form):
    tanggal = forms.CharField(label="Tanggal", max_length=50)
    email = forms.CharField(label="Email", max_length=50)
    nominal = forms.IntegerField(label="Nominal")

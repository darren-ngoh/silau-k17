from django.shortcuts import render, redirect
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from .forms import CreateTopup, UpdateTopup
from datetime import date

# Create your views here.
def rud_topup(request):
    if "is_kurir" in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')

    if "is_staf" in request.session:
        response = {}
        response['data'] = get_daftar_topup_staf()
        return render(request, 'rud_topup.html', response)
    elif "is_user" in request.session:
        response = {}
        response['data'] = get_daftar_topup(request)
        return render(request, 'rud_topup.html', response)

def c_topup(request):
    if "is_kurir" in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')

    response = {}
    response['error'] = False
    all_email = get_all_email()
    form = CreateTopup(request.POST or None)
    form.fields['email'].choices = all_email
    response['form'] = form

    if request.method == "POST" and form.is_valid():
        email = form.cleaned_data['email']
        nominal = form.cleaned_data['nominal']
        tanggal = date.today()
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute(f"insert into transaksi_topup (email, tanggal, nominal) values('{email}','{tanggal}','{nominal}');")
        return redirect('/dpay')
    return render(request, 'c_topup.html', response)

def get_daftar_topup(request):
    email = request.session['email']
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(f"select * from transaksi_topup where email='{email}';")
    daftar_topup_response = fetch(cursor)
    return daftar_topup_response

def get_daftar_topup_staf():
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from transaksi_topup")
    daftar_topup_response = fetch(cursor)
    return daftar_topup_response

def get_all_email():
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select email from pelanggan;")
    emails_response = fetch(cursor)
    res=[]
    for email in emails_response:
        temp=email['email']
        res.append((temp,temp,))
    return res

@csrf_exempt
def update_topup(request, email, tanggal, nominal):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    
    response = {}
    response['error'] = False
    form = UpdateTopup(request.POST or None, initial={'email':email, 'tanggal':tanggal, 'nominal':nominal})
    form.fields['email'].disabled = True
    form.fields['tanggal'].disabled = True
    response['form'] = form

    if request.method == "POST" and form.is_valid():
        new_nominal = form.cleaned_data['nominal']
        cursor = connection.cursor()
        cursor.execute('set search_path to silau;')
        cursor.execute(f"update transaksi_topup set nominal='{new_nominal}' where email='{email}' and tanggal='{tanggal}' and nominal='{nominal}'")
        return redirect('/dpay')
    
    return render(request,'update_topup.html', response)


def delete_topup(request, email, tanggal, nominal):
    cursor = connection.cursor()
    cursor.execute('set search_path to silau;')
    cursor.execute(f"delete from transaksi_topup where email='{email}' and tanggal='{tanggal}' and nominal='{nominal}'")
    return redirect("/dpay")


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]
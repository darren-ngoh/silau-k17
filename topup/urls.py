from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='topup'

urlpatterns = [ 
	path('', views.rud_topup), 
	path('create/', views.c_topup),
	path('update/<str:email>/<str:tanggal>/<str:nominal>', views.update_topup),
	path('delete/<str:email>/<str:tanggal>/<str:nominal>', views.delete_topup),
]
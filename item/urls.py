from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='item'

urlpatterns = [
	path('',views.display_item,name='item'),
    path('create/',views.create_item,name='create-item'),
]

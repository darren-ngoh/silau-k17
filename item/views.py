from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import CreateItemForm
import json
from django.views.decorators.csrf import csrf_exempt

def create_item(request):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')

    response={}
    response['error']=False
    form = CreateItemForm(request.POST or None)
    response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        print(form.cleaned_data)
        kode = form.cleaned_data['kode']
        nama = form.cleaned_data['nama']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("insert into item (kode,nama) values(%s,%s);",[kode,nama])
        return redirect('/items')
    return render(request,'create-item.html',response)

def display_item(request):
    if "email" not in request.session: 
        return redirect('/auth/login')
    response={}
    response['data'] = get_item()
    response['is_staf'] = "is_staf" in request.session
    return render(request,'display-item.html',response)

def get_item():
    query = """ 
    select * from item;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    item_response = fetch(cursor)
    return item_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

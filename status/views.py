from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def display_status(request):
    if "email" not in request.session:
        return redirect('/auth/login')
    response = {}
    response['data'] = get_status()
    return render(request, 'list_status.html', response)

def get_status():
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from status")
    return fetch(cursor)

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]
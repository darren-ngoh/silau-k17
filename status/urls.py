from django.conf.urls import url
from django.urls import include,path
from . import views

app_name = 'status'

urlpatterns = [
	path('', views.display_status)
]

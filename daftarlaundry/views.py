from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import UpdateDaftarLaundry, CreateDaftarLaundry
import json
from django.views.decorators.csrf import csrf_exempt
from datetime import date

def create_daftar_laundry(request):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized') 
    if "email" not in request.session:
        return redirect('/auth/login')

    response={}
    response['error']=False
    kodeku=get_kode_item()
    emailku=get_all_email()
    form = CreateDaftarLaundry(request.POST or None)
    form.fields['kode_item'].choices=kodeku
    form.fields['email'].choices=emailku
    response['form'] = form
    
    if(request.method == "POST" and form.is_valid()):
        print(form.cleaned_data)
        email = form.cleaned_data['email']
        jumlah_item = form.cleaned_data['jumlah_item']
        kode_item = form.cleaned_data['kode_item']
        today = date.today()
        no_urut=get_no_urut(email,today)
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("insert into daftar_laundry (email,tanggal_transaksi,no_urut,jumlah_item,kode_item) values(%s,%s,%s,%s,%s);",[email,today,no_urut,jumlah_item,kode_item])
        return redirect('/daftarlaundry') 
    return render(request,'create-daftar-laundry.html',response)

def display_daftar_laundry(request):
    if "is_kurir" in request.session:
        return redirect('/auth/unauthorized')
    if "email" not in request.session:
        return redirect('/auth/login')

    if "is_staf" in request.session:
        response={}
        response['data'] = get_daftar_laundry_staf()
        return render(request,'display-daftar-laundry.html',response)

    elif "is_user" in request.session:
        response={}
        response['data'] = get_daftar_laundry(request)
        return render(request,'display-daftar-laundry.html',response)

def get_daftar_laundry_staf():
    query = """
    select * from daftar_laundry;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute(query)
    daftar_laundry_response = fetch(cursor)
    return daftar_laundry_response

def get_daftar_laundry(request):
    email = request.session["email"]
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from daftar_laundry where email=%s;",[email])
    daftar_laundry_response = fetch(cursor)
    return daftar_laundry_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_daftar_laundry(request,email,tanggal_transaksi,no_urut):
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("delete from daftar_laundry where email=%s and tanggal_transaksi=%s and no_urut=%s;",[email,tanggal_transaksi,no_urut])
        return redirect("/daftarlaundry")

@csrf_exempt
def update_daftar_laundry(request,email,tanggal_transaksi,no_urut):
    if "is_staf" not in request.session:
        return redirect('/auth/unauthorized')
    if(email != None):
        prefilled_data = get_data(email,tanggal_transaksi,no_urut)
        response={}
        response['error']=False
        kodeku = get_kode_item()
        form = UpdateDaftarLaundry(request.POST or None, initial={'email':email,'tanggal':tanggal_transaksi,'no_urut':no_urut,'jumlah_item':prefilled_data['jumlah_item'],'kode_item':prefilled_data['kode_item']})
        form.fields['kode_item'].choices=kodeku
        print(kodeku)
        form.fields['email'].disabled = True
        form.fields['tanggal'].disabled = True
        form.fields['no_urut'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        email = form.cleaned_data['email']
        tanggal_transaksi = form.cleaned_data['tanggal']
        no_urut = form.cleaned_data['no_urut']
        jumlah = form.cleaned_data['jumlah_item']
        kode = form.cleaned_data['kode_item']
        cursor = connection.cursor()
        cursor.execute("set search_path to silau;")
        cursor.execute("update daftar_laundry set kode_item=%s, jumlah_item=%s where email=%s and tanggal_transaksi=%s and no_urut=%s",[kode,jumlah,email,tanggal_transaksi,no_urut])
        return redirect("/daftarlaundry")
    return render(request,'update-daftarlaundry.html',response)

def get_data(email,tanggal_transaksi,no_urut):
    cursor = connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select * from daftar_laundry where email=%s and tanggal_transaksi=%s and no_urut=%s;",[email,tanggal_transaksi,no_urut])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_kode_item():
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select kode from item;")
    kode_item_response = fetch(cursor)
    res=[]
    for kode_item in kode_item_response:
        temp=kode_item['kode']
        res.append((temp,temp,))
    return res

def get_all_email():
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select email from transaksi_laundry;")
    emails_response = fetch(cursor)
    res=[]
    for email in emails_response:
        temp=email['email']
        res.append((temp,temp,))
    return res

def get_no_urut(email,today):
    cursor=connection.cursor()
    cursor.execute("set search_path to silau;")
    cursor.execute("select no_urut from daftar_laundry where email=%s and tanggal_transaksi=%s;",[email,today])
    urutan_response = fetch(cursor)
    temp=[]
    for urutan in urutan_response:
        temp.append(urutan['no_urut'])
    if len(temp)==0:
        return 1
    res=int(max(temp))+1
    return res
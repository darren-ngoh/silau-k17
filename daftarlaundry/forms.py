from django import forms

class UpdateDaftarLaundry(forms.Form):
    email=forms.EmailField(label="Email",max_length=50)
    tanggal=forms.CharField(label='Tanggal Transaksi',max_length=50)
    no_urut=forms.IntegerField(label='Nomor Urut')
    jumlah_item=forms.IntegerField(label='Jumlah Item')
    kode_item = forms.ChoiceField(label='Kode Item',choices=[]) 

class CreateDaftarLaundry(forms.Form):
    email=forms.ChoiceField(label="Email",choices=[])
    jumlah_item=forms.IntegerField(label='Jumlah Item')
    kode_item = forms.ChoiceField(label='Kode Item',choices=[]) 
from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='daftarlaundry'

urlpatterns = [ 
	path('',views.display_daftar_laundry,name='display-daftar-laundry'), 
	path('create/',views.create_daftar_laundry,name='create-daftar-laundry'), 
	path('update/<str:email>/<str:tanggal_transaksi>/<str:no_urut>',views.update_daftar_laundry,name='update-daftar-laundry'), 
	path('delete/<str:email>/<str:tanggal_transaksi>/<str:no_urut>',views.delete_daftar_laundry,name='delete-daftar-laundry'),
] 
 